#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings


// Stucture to hold both a plaintext password and a hash
typedef struct {
    char *word;
    char *hash;
} Entry;

// comparison function for qsort
// compares hashes within dictionary
int stringCmp(const void *a, const void *b) {
    Entry *aa = (Entry*)a;  
    Entry *bb = (Entry*)b;  
    return strcmp(aa->hash, bb->hash);
}

// comparison function for bsearch
// compares target hash and hash from dictionary
int hashCmp(const void *key, const void *element) {
    char *kk = (char *) key;
    Entry *ee = (Entry *) element;
    return strcmp(kk, ee->hash);
}

// Read in the dictionary file and return an array of structs.
// Each entry should contain both the hash and the dictionary
// word.
Entry *read_dictionary(char *filename, int *size)
{   
    // find filesize
    struct stat info;
    if ( stat(filename, &info) == -1 ) {
        puts("Can't stat the file");
        exit(1);
    }
    int filesize = info.st_size;
    
    // open dictionary file for reading
    FILE *dictFile;
    if ((dictFile = fopen(filename, "rb")) == NULL) {
        printf("File %s could not be opened for reading\n", filename);
        exit(1);
    }
    
    // read contents of the file into 
    // allocated memory
    char *contents = malloc(filesize + 1);
    fread(contents, 1, filesize, dictFile);
    fclose(dictFile);
    
    contents[filesize] = '\0';
    
    // count number of lines 
    *size = 0;
    for (int i = 0; i < filesize; i++) {
        if (contents[i] == '\n') {
            (*size)++;
        }
    }
    
    // allocate memory for array of structures (dictionary)
    Entry *dictionary = malloc(sizeof(Entry) * (*size));
    
    // first element of array
    int k = 1;
    dictionary[0].word = strtok(contents, "\n");
    dictionary[0].hash = md5(dictionary[0].word, strlen(dictionary[0].word));
    
    // other elements of array
    while ((dictionary[k].word = strtok(NULL, "\n")) != NULL) {
        dictionary[k].hash = md5(dictionary[k].word, strlen(dictionary[k].word));
        k++;
    }

    return dictionary;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the dictionary file into an array of entry structures
    int dsize;
    Entry *dict = read_dictionary(argv[2], &dsize);
    char * to_free = dict[0].word;

    // Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    qsort(dict, dsize, sizeof(Entry), stringCmp);
        

    // Open the hash file for reading.
    FILE *hashFile = fopen(argv[1], "r");
    
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary word.
    // Print out both the hash and word.
    char hash[34];
    Entry *found;
    while ( fgets(hash, 34, hashFile) != NULL) {
        *strchr(hash, '\n') = '\0';

        found = bsearch(hash, dict, dsize, sizeof(Entry), hashCmp);
        if (found) {
            printf("Found hash\t%s\t for password\t%s\n", found->hash, found->word);
        }
        else {
            puts("Didn't find hash in the dictionary.");
        }
    }
    
    fclose(hashFile);
    
    // free heap memory
    free(to_free);
    for (int i = 0; i < dsize; i++) {
        free(dict[i].hash);
    }
    
    free(dict);
}
